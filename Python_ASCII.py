"""
Emanuel Navarro-Ortiz
016449108
emanuel.navarro-ortiz@sjsu.edu
due 10/2/22
Working Status: Working and tested 10/2/22
"""

def isUpper(letter: str) -> bool:
  return ( ord(letter) > 64) and (ord(letter) < 91)

def isLower(letter: str) -> bool:
  return ( ord(letter) > 96) and (ord(letter) < 123)

def isInAlphabet(letter: str) -> bool:
  return isUpper(letter) or isLower(letter)

def isDigit(letter: str) -> bool:
  return ( ord(letter) > 47 ) and ( ord(letter) < 58)

def isASpecialCharacter(letter: str) -> bool:
  return ((ord(letter) > 32) and (ord(letter) < 48)) or ((ord(letter) > 57 ) and (ord(letter) < 65)) or ((ord(letter) > 90 ) and (ord(letter) < 97)) or ((ord(letter) > 122 ) and (ord(letter) < 127))

#assertion test for isUpper, isLower, and isInAlphabet
for i in range(ord('A'), ord('Z')+1 ):
  assert isUpper(chr(i)) == True
  assert isLower(chr(i + 32)) == True
  assert isInAlphabet(chr(i)) == True
  assert isInAlphabet(chr(i + 32 )) == True

# asserting each special character is included
for i in range(ord('!'), ord('/')+1 ):
  assert isASpecialCharacter(chr(i)) == True
for i in range(ord(':'), ord('@')+1 ):
  assert isASpecialCharacter(chr(i)) == True
for i in range(ord('['), 97 ):
  assert isASpecialCharacter(chr(i)) == True
for i in range(ord('{'), ord('~')+1 ):
  assert isASpecialCharacter(chr(i)) == True


